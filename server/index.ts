import Server from './Server.js';
import loadEnvVars from './loadEnvVars.js';

loadEnvVars();

const server = new Server();
server.startServer();
