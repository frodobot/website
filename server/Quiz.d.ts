export interface Quiz {
	quizId: string;
	name: string;
	image?: string;
	description: string;
	categoryId: string;
	difficultyId: string;
	lastChanged: string;
}

export interface QuizData {
	quizId: string;
	userId: string;
	name: string;
	image?: string;
	description: string;
	questions: Question[];
	categoryId: string;
	difficultyId: string;
	lastChanged: string;
}

interface Question {
	questionId: string;
	question: string;
	incorrectAnswers: string[];
	correctAnswers: string[];
}

export interface DatabaseQuizResponse {
	userId: string;
	name: string;
	description: string;
	image: string | undefined;
	categoryId: string;
	difficultyId: string;
	lastChanged: string;
	questionId: number;
	question: string;
	answer: string;
	correct: number;
}
