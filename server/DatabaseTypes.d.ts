export interface Difficulty {
	id: number;
	name: string;
	active: boolean;
}
export interface Category {
	id: number;
	name: string;
	active: boolean;
}
