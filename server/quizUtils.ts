import {QuizData} from './Quiz.d';

export function validateQuiz(quiz: QuizData): boolean {
	if (!quiz.name?.trim() || quiz.name?.trim().length < 5 || quiz.name?.trim().length > 30 ||
		!quiz.description?.trim() || quiz.description?.trim().length < 10 || quiz.description?.trim().length > 300 ||
		!quiz.categoryId ||
		!quiz.difficultyId ||
		!quiz.questions || quiz.questions.length < 2 || quiz.questions.length > 20 ||
		quiz.questions.some((question) => (
			!question.question?.trim() || question.question?.trim().length > 100 ||
			!question.incorrectAnswers || question.incorrectAnswers.length < 1 || !question.incorrectAnswers.some((answer) => answer.trim().length >= 1) ||
			!question.correctAnswers || question.correctAnswers.length < 1 || !question.correctAnswers.some((answer) => answer.trim().length >= 1)
		))
	) {
		return false;
	}
	return true;
}
