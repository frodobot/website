import {Connection, createConnection, OkPacket} from 'mysql';

import {LeaderboardItem} from './LeaderboardItem.d';
import Server from './Server';
import {User} from './discord/Discord';
import {QuizData, DatabaseQuizResponse} from './Quiz.d';
import {Difficulty, Category} from './DatabaseTypes';

export default class Database {
	server: Server;
	connection: Connection;
	connected: boolean;
	reconnecting: boolean;

	constructor(server: Server) {
		this.server = server;
		this.connected = false;
		this.reconnecting = false;
		if (!process.env.DB_PASSWORD) {
			this.server.warnLog('No database password set, not connecting to database.');
			return;
		}
		this.connect();
		this.beginPing();
	}

	private async connect() {
		if (this.connected) return;
		this.reconnecting = true;
		try {
			this.connection = createConnection({
				host: process.env.DB_SERVER,
				user: process.env.DB_USER,
				password: process.env.DB_PASSWORD,
				database: process.env.DB_DATABASE,
				port: parseInt(process.env.DB_PORT || '3306'),
				charset: 'utf8mb4',
			});

			this.connection.connect((err) => {
				this.reconnecting = false;
				if (err) {
					this.server.errorLog('Failed to connect to database', err);
					return;
				}
				this.connected = true;
				this.server.debugLog('Successfully connected to database');
			});
		} catch (err) {
			this.server.errorLog('Failed to connect to database', err);
		}
	}

	private beginPing() {
		if (!this.connected) return;
		setInterval(() => {
			try {
				this.connection.ping((err) => {
					if (err) {
						this.server.errorLog('Failed to ping database', err);
						if (err.fatal && !this.reconnecting) {
							try {
								this.server.errorLog('Fatal error in database connection, reconnecting...');
								this.connection.end();
								this.connected = false;
								this.connect();
							} catch (err) {}
						}
					}
				});
			} catch (err) {}
		}, 300000);
	}

	public query<T>(query: string, values: any[] = []): Promise<T> {
		if (!this.connected) return Promise.reject(new Error('Not connected to database'));
		return new Promise((resolve, reject) => {
			this.connection.query(query, values, (err, data) => {
				if (err) {
					if (err.fatal && !this.reconnecting) {
						try {
							this.server.errorLog('Fatal error in database connection, reconnecting...');
							this.connection.end();
							this.connected = false;
							this.connect();
						} catch (err) {}
					}
					return reject(err);
				}
				resolve(data);
			});
		});
	}

	public async getTopLeaderboard(game: string): Promise<LeaderboardItem[]> {
		try {
			const data = await this.query<LeaderboardItem[]>(`
				SELECT
					cast(u.userId as char) as userId,
					u.name,
					u.avatar,
					u.discriminator,
					l.score
				FROM leaderboards l
				LEFT JOIN users u ON u.userId = l.userId
				WHERE l.game = ?
				ORDER BY l.score DESC
				LIMIT 10
			`, [game]);
			return data;
		} catch (err) {
			this.server.errorLog('Failed to get top leaderboard', err);
			throw err;
		}
	}

	static makeRandomId(length = 5) {
		let result = '';
		const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		const charactersLength = characters.length;
		for (let i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	private async generateQuizId(): Promise<string> {
		const quizId = Database.makeRandomId();
		const exists = await this.query<QuizData[]>(`SELECT * FROM quizzes WHERE quizId = ?`, [quizId]);
		if (exists.length) return this.generateQuizId();
		return quizId;
	}

	public async makeQuiz(quiz: QuizData, user: User): Promise<string> {
		try {
			const quizId = await this.generateQuizId();
			await this.query(`
				INSERT INTO quizzes (
					quizId,
					displayId,
					userId,
					name,
					description,
					image,
					categoryId,
					difficultyId,
					lastChanged
				) VALUES (?, ?, ?, ?, ?, ?, ?, ?, now());
			`, [
				quizId,
				quizId,
				user.id,
				quiz.name,
				quiz.description,
				quiz.image,
				quiz.categoryId,
				quiz.difficultyId,
			]);
			for (const question of quiz.questions) {
				const query = await this.query<OkPacket>(`
					INSERT INTO questions (
						quizId,
						question
					) VALUES (?, ?);
				`, [
					quizId,
					question.question,
				]);
				const questionId = query.insertId;
				for (const answer in question.incorrectAnswers) {
					if (question.incorrectAnswers.hasOwnProperty(answer)) {
						await this.query(`
							INSERT INTO answers (
								questionId,
								answer,
								correct
							) VALUES (?, ?, false);
						`, [
							questionId,
							question.incorrectAnswers[answer],
						]);
					}
				}
				await this.query(`
					INSERT INTO answers (
						questionId,
						answer,
						correct
					) VALUES (?, ?, true);
				`, [
					questionId,
					question.correctAnswers[0],
				]);
			}
			return quizId;
		} catch (err) {
			this.server.errorLog('Failed to make quiz', err);
			throw err;
		}
	}

	public async getQuiz(quizId: string): Promise<QuizData | null> {
		try {
			const data = await this.query<DatabaseQuizResponse[]>(`
				SELECT
					cast(quizzes.userId as char) as userId,
					quizzes.name,
					quizzes.description,
					quizzes.image,
					quizzes.categoryId,
					quizzes.difficultyId,
					(UNIX_TIMESTAMP(quizzes.lastChanged) * 1000) AS lastChanged,
					questions.questionId,
					questions.question,
					answers.answer,
					answers.correct
				FROM quizzes
				LEFT JOIN questions ON questions.quizId = quizzes.quizId
				LEFT JOIN answers ON answers.questionId = questions.questionId
				WHERE quizzes.displayId = ?
			`, [quizId]);

			if (!data.length) return null;

			const usedQuestionIds: number[] = [];
			const quiz: QuizData = {
				quizId,
				userId: data[0].userId,
				name: data[0].name,
				description: data[0].description,
				image: data[0].image,
				categoryId: data[0].categoryId,
				difficultyId: data[0].difficultyId,
				questions: [],
				lastChanged: data[0].lastChanged,
			};
			data.forEach((question) => {
				if (!usedQuestionIds.includes(question.questionId)) {
					usedQuestionIds.push(question.questionId);
					quiz.questions.push({
						questionId: question.questionId.toString(),
						question: question.question,
						correctAnswers: [],
						incorrectAnswers: [],
					});
				}
				if (question.correct) {
					quiz.questions[quiz.questions.length - 1].correctAnswers.push(question.answer);
				} else {
					quiz.questions[quiz.questions.length - 1].incorrectAnswers.push(question.answer);
				}
			});
			return quiz;
		} catch (err) {
			this.server.errorLog('Failed to get quiz', err);
			throw err;
		}
	}

	public async editQuiz(displayId: string, quiz: QuizData, user: User): Promise<void> {
		try {
			const [{userId, quizId}] = await this.query<{userId: string, quizId: string}[]>(`
				SELECT cast(userId as char) as userId, quizId FROM quizzes WHERE displayId = ?
			`, [displayId]);

			if (userId !== user.id) {
				throw new Error('Quiz not owned by user');
			}

			await this.query(`
				UPDATE quizzes
				SET	
					name = ?,
					description = ?,
					image = ?,
					categoryId = ?,
					difficultyId = ?,
					lastChanged = now()
				WHERE quizId = ? AND userId = ?;
			`, [
				quiz.name,
				quiz.description,
				quiz.image,
				quiz.categoryId,
				quiz.difficultyId,
				quizId,
				user.id,
			]);
			await this.query(`
				DELETE FROM answers WHERE questionId IN (SELECT questionId FROM questions WHERE quizId = ?);
			`, [quizId]);
			await this.query(`
				DELETE FROM questions WHERE quizId = ?;
			`, [quizId]);
			for (const question of quiz.questions) {
				const query = await this.query<OkPacket>(`
					INSERT INTO questions (
						quizId,
						question
					) VALUES (?, ?);
				`, [
					quizId,
					question.question,
				]);
				const questionId = query.insertId;
				for (const answer in question.incorrectAnswers) {
					if (question.incorrectAnswers.hasOwnProperty(answer)) {
						await this.query(`
							INSERT INTO answers (
								questionId,
								answer,
								correct
							) VALUES (?, ?, false);
						`, [
							questionId,
							question.incorrectAnswers[answer],
						]);
					}
				}
				await this.query(`
					INSERT INTO answers (
						questionId,
						answer,
						correct
					) VALUES (?, ?, true);
				`, [
					questionId,
					question.correctAnswers[0],
				]);
			}
		} catch (err) {
			this.server.errorLog('Failed to edit quiz', err);
			throw err;
		}
	}

	public async getUserQuizzes(userId: string): Promise<QuizData[]> {
		try {
			const data = await this.query<QuizData[]>(`
				SELECT
					displayId as quizId,
					name,
					description,
					image,
					categoryId,
					difficultyId,
					(UNIX_TIMESTAMP(lastChanged) * 1000) AS lastChanged
				FROM quizzes
				WHERE userId = ?
			`, [userId]);
			return data;
		} catch (err) {
			this.server.errorLog('Failed to get user quizzes', err);
			throw err;
		}
	}

	public async getCategories(): Promise<Category[]> {
		try {
			const data = await this.query<Category[]>(`
				SELECT * FROM quizCategories
			`);
			return data;
		} catch (err) {
			this.server.errorLog('Failed to get categories', err);
			throw err;
		}
	}

	public async getDifficulties(): Promise<Difficulty[]> {
		try {
			const data = await this.query<Difficulty[]>(`
				SELECT * FROM quizDifficulties
			`);
			return data;
		} catch (err) {
			this.server.errorLog('Failed to get difficulties', err);
			throw err;
		}
	}

	public async deleteQuiz(quizId: string): Promise<OkPacket> {
		try {
			return await this.query<OkPacket>(`
				DELETE FROM quizzes WHERE displayId = ?;
			`, [quizId]);
		} catch (err) {
			this.server.errorLog('Failed to delete quiz', err);
			throw err;
		}
	}

	public async userIsAdmin(userId: string): Promise<boolean> {
		try {
			const data = await this.query<{admin: number}[]>(`
				SELECT admin FROM users WHERE userId = ?
			`, [userId]);
			return Boolean(data[0]?.admin);
		} catch (err) {
			this.server.errorLog('Failed to get user admin status', err);
			throw err;
		}
	}

	async getUserQuizCount(userId: string): Promise<number> {
		try {
			const data = await this.query<{count: number}[]>(`
				SELECT COUNT(*) AS count FROM quizzes WHERE userId = ?
			`, [userId]);
			return data[0]?.count;
		} catch (err) {
			this.server.errorLog('Failed to get user quiz count', err);
			throw err;
		}
	}

	async addUser(user: User): Promise<void> {
		try {
			await this.query(`
				INSERT INTO users (
					userId,
					name,
					discriminator,
					avatar
				) VALUES (
					?,
					?,
					?,
					?
				) ON DUPLICATE KEY UPDATE
					name = ?,
					discriminator = ?,
					avatar = ?
			`, [
				user.id,
				user.username,
				user.discriminator,
				user.avatar,
				user.username,
				user.discriminator,
				user.avatar,
			]);
		} catch (err) {
			this.server.errorLog('Failed to add user', err);
		}
	}
}
