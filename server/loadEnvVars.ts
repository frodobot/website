// Used to ensure the environment variables are loaded from the correct location
// So the file can be executed from anywhere

import {config} from 'dotenv';
import * as url from 'url';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

export default function loadEnvVars() {
	config({path: `${__dirname}/../.env`});
}

export {__dirname};
