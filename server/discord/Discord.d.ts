export interface TokenData {
	access_token: string;
	expires_in: number;
	refresh_token: string;
	scope: string;
	token_type: string;
}
export interface UserData {
	id: string;
	username: string;
	avatar: string;
	discriminator: string;
	public_flags: string,
	flags: string,
	banner: string,
	banner_color: string,
	accent_color: string,
	locale: string,
	mfa_enabled: string,
}
export interface User {
	id: string;
	username: string;
	discriminator: string;
	avatar: string;
	admin: boolean;
}
