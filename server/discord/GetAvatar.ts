export default function getAvatar(userId: string, avatarHash: string, discriminator: number): string {
	if (avatarHash) return `https://cdn.discordapp.com/avatars/${userId}/${avatarHash}.${avatarHash.startsWith('a_') ? 'gif' : 'png'}`;
	return `https://cdn.discordapp.com/embed/avatars/${(discriminator % 5) || 0}.png`;
}
