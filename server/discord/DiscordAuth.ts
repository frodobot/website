import {TokenData, UserData} from './Discord.d';
import {Request} from 'express';
import fetch from 'node-fetch';

export async function discordAuth(code: string, req: Request): Promise<UserData> {
	try {
		const tokenReq = await fetch('https://discord.com/api/v8/oauth2/token', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			body: new URLSearchParams({
				'client_id': '734746193082581084',
				'client_secret': process.env.CLIENTSECRET || '',
				'grant_type': 'authorization_code',
				'code': code,
				'redirect_uri': `${req.get('X-Forwarded-Proto') || 'http'}://${req.hostname}/api/discord/auth`,
			}),
		});
		if (tokenReq.status !== 200) throw new Error('Invalid token request');
		const token = <TokenData> await tokenReq.json();
		const userReq = await fetch('https://discord.com/api/users/@me', {
			headers: {
				authorization: `${token.token_type} ${token.access_token}`,
			},
		});
		if (userReq.status !== 200) throw new Error('Invalid user request');
		const user = <UserData> await userReq.json();
		return user;
	} catch (err) {
		console.log(err);
		throw err;
	}
}
