export interface LeaderboardItem {
	userId?: string;
	avatar: string;
	score: string;
	discriminator?: number;
	name: string;
}
