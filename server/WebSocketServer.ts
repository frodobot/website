import Server from './Server.js';
import {WebSocketServer as WsServer, WebSocket} from 'ws';

interface FrodoStats {
	servers: string;
	users: string;
	online: boolean;
	shards: string;
}

export default class WebSocketServer {
	server: Server;
	wsServer: WsServer;
	socket: WebSocket | null;
	lastMessageReceived: number;
	authTokens: string[];

	dataStats: FrodoStats;

	constructor(server: Server) {
		this.server = server;
		this.authTokens = [];
		this.wsServer = new WsServer({
			server: this.server.server,
		});
		this.dataStats = {
			servers: '0',
			users: '0',
			online: false,
			shards: '0',
		};
		this.registerEvents();
	}

	public addNewAuthToken(): string {
		const token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
		this.authTokens.push(token);
		return token;
	}

	private registerEvents() {
		this.wsServer.on('connection', (socket, req) => {
			this.server.debugLog('WebSocket Connection established');
			const token = req.url?.replace('/', '') || '';
			const authorized = this.authTokens.includes(token);
			delete this.authTokens[this.authTokens.indexOf(token)];
			if (!authorized) {
				this.server.warnLog('WebSocket Invalid authentication');
				socket.close();
				return;
			}
			this.socket = socket;
			this.lastMessageReceived = Date.now();

			socket.on('message', (data) => {
				this.lastMessageReceived = Date.now();
				const [servers, users, shards] = data.toString().split(':');
				this.dataStats.servers = servers;
				this.dataStats.users = users;
				this.dataStats.online = true;
				this.dataStats.shards = shards;
				socket.send(JSON.stringify({
					type: 'pong',
				}));
			});

			socket.on('close', () => {
				this.socket = null;
				this.server.warnLog('WebSocket Connection closed');
			});
		});
	}

	get stats(): FrodoStats {
		if (this.lastMessageReceived + 60000 < Date.now()) {
			this.dataStats.online = false;
		}
		return this.dataStats;
	}
}
