import chalk from 'chalk';

import express, {Express} from 'express';
import http from 'http';
import https from 'https';
import fs from 'fs';
import session from 'express-session';
import ImageKit from 'imagekit';
import path from 'path';
import {randomBytes} from 'crypto';

import WebSocketServer from './WebSocketServer.js';
import {discordAuth} from './discord/DiscordAuth.js';
import getAvatar from './discord/GetAvatar.js';
import Database from './Database.js';
import {User} from './discord/Discord';
import {validateQuiz} from './quizUtils.js';
import {__dirname} from './loadEnvVars.js';

// Define the session data for typescript
declare module 'express-session' {
	interface SessionData {
		user?: User;
		redirect?: string;
	}
}

function getPath(url: string): string {
	return path.join(process.cwd(), url);
}

export default class Server {
	app: Express;
	server: http.Server | https.Server;
	regexPagesTest: RegExp;
	webSocketServer: WebSocketServer;
	imagekit: ImageKit;
	database: Database;

	constructor() {
		this.app = express();
		this.server = this.createServer();
		this.regexPagesTest = /^(\/|\/feedback|\/commands|\/leaderboard|\/legal|\/quizzes|\/quizzes\/.*|\/admin)$/;
		this.webSocketServer = new WebSocketServer(this);
		this.database = new Database(this);
		this.imagekit = new ImageKit({
			publicKey: process.env.IMAGEKITPUBLICKEY || '',
			privateKey: process.env.IMAGEKITPRIVATEKEY || '',
			urlEndpoint: process.env.IMAGEKITURLENDPOINT || '',
		});

		this.debugLog('Setting up redirects');
		this.setUpredirects();
		this.debugLog('Setting up routes');
		this.setUpRoutes();
	}

	private setUpredirects() {
		this.app.use((req, res, next) => {
			const protocal = req.get('X-Forwarded-Proto');
			const host = req.get('Host');
			if ((protocal !== 'https' && host === 'frodo.fun') || host === 'www.frodo.fun' || host === 'frodowebsite.herokuapp.com') return res.redirect(`https://frodo.fun${req.url}`);
			else if (host === 'i.frodo.fun' || host === 'invite.frodo.fun') res.redirect(301, 'https://discord.com/oauth2/authorize?client_id=734746193082581084&permissions=268822608&scope=bot%20applications.commands');
			else if (host === 'slash.frodo.fun') res.redirect(301, 'https://discord.com/api/oauth2/authorize?client_id=734746193082581084&scope=applications.commands');
			else if (host === 'help.frodo.fun') res.redirect(301, 'https://frodo.fun/feedback');
			else if (host === 'support.frodo.fun') res.redirect('https://discord.gg/dQhYjvTud7');
			else next();
		});
	}

	private setUpRoutes() {
		this.app.set('trust proxy', 1);
		this.app.use(session({
			secret: randomBytes(64).toString('hex'),
			resave: false,
			saveUninitialized: true,
			cookie: {
				secure: process.env.RUNTIME ? true : false,
			},
		}));
		this.app.use(express.static(getPath('build')));
		this.app.use(express.json());
		this.app.get('*', async (req, res) => {
			try {
				const url = req.url.split('?')[0];

				switch (url) {
				case '/api/leaderboard/trivia':
					const leaderboard = await this.database.getTopLeaderboard('trivia');
					if (!leaderboard) {
						res.status(500).json({error: 'Internal server error'});
					} else {
						leaderboard.forEach((user) => {
							user.avatar = getAvatar(user.userId!, user.avatar, user.discriminator!);
							delete user.discriminator;
							delete user.userId;
						});
						res.status(200).json(leaderboard);
					}
					return;
				case '/api/websockettoken':
					if (req.headers.authorization !== process.env.WEBSOCKETAUTH) {
						return res.status(401).json({error: 'Unauthorized'});
					} else {
						return res.status(200).json({token: this.webSocketServer.addNewAuthToken()});
					}
				case '/api/status':
					return res.status(200).json(getStatusSheildJson(this.webSocketServer.stats.online));
				case '/api/servers':
					return res.status(200).json(getServerSheildJson('Servers', this.webSocketServer.stats.servers));
				case '/api/users':
					return res.status(200).json(getServerSheildJson('Users', this.webSocketServer.stats.users));
				case '/api/shards':
					return res.status(200).json({count: this.webSocketServer.stats.shards});
				case '/api/discord/auth':
					const code = req.query.code?.toString();
					if (code) {
						const user = await discordAuth(code, req);
						if (user) {
							try {
								const userIsAdmin = await this.database.userIsAdmin(user.id);
								const userData = {
									id: user.id,
									username: user.username,
									discriminator: user.discriminator,
									avatar: user.avatar,
									admin: userIsAdmin,
								};
								this.database.addUser(userData);
								userData.avatar = getAvatar(userData.id, userData.avatar, parseInt(userData.discriminator));
								req.session.user = userData;
								const redirect = req.session.redirect || '/';
								res.redirect(`${redirect}?loggedIn`);
							} catch (err) {
								this.errorLog('Failed to log user in', err);
								res.status(500).redirect('/');
							}
						} else {
							res.status(500).json({error: 'Internal server error'});
						}
						return;
					} else {
						const redirect = String(req.query.redirect || '/');
						req.session.redirect = redirect;
						return res.redirect(`https://discord.com/api/oauth2/authorize?client_id=734746193082581084&redirect_uri=${req.get('X-Forwarded-Proto') || 'http'}://${req.hostname}/api/discord/auth&response_type=code&scope=identify`);
					}
				case '/api/discord/me':
					if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
					return res.status(200).json(req.session.user);
				case '/api/discord/logout':
					req.session.user = undefined;
					const redirect = String(req.query.redirect);
					return res.redirect(`${redirect || '/'}?loggedOut`);
				case '/api/quizzes':
					if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
					const quizId = String(req.query.quizId);
					if (!quizId) return res.status(400).json({error: 'No quizId'});
					try {
						const quiz = await this.database.getQuiz(quizId);
						if (!quiz) return res.status(404).json({error: 'Quiz not found'});
						if (quiz.userId !== req.session.user.id) return res.status(403).json({error: 'Not quiz owner'});
						return res.status(200).json(quiz);
					} catch (err) {
						this.errorLog('Error getting quiz', err);
						return res.status(500).json({error: 'Internal server error'});
					}
				case '/api/quizzes/me':
					if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
					try {
						const quizzes = await this.database.getUserQuizzes(req.session.user.id);
						return res.status(200).json(quizzes || []);
					} catch (err) {
						this.errorLog('Error getting user quizzes', err);
						return res.status(500).json({error: 'Internal server error'});
					}
				case '/api/quizzes/categories':
					try {
						const categories = await this.database.getCategories();
						return res.status(200).json(categories || []);
					} catch (err) {
						this.errorLog('Error getting categories', err);
						return res.status(500).json({error: 'Internal server error'});
					}
				case '/api/quizzes/difficulties':
					try {
						const difficulties = await this.database.getDifficulties();
						return res.status(200).json(difficulties || []);
					} catch (err) {
						this.errorLog('Error getting difficulties', err);
						return res.status(500).json({error: 'Internal server error'});
					}
				case '/api/imagekit':
					const authentication = this.imagekit.getAuthenticationParameters();
					return res.status(200).json({
						...authentication,
						publicKey: process.env.IMAGEKITPUBLICKEY,
					});
				default:
					if (this.regexPagesTest.test(url)) {
						res.status(200);
					} else {
						res.status(404);
					};
					res.sendFile(getPath('/build/index.html'));
					break;
				};
			} catch (err) {
				this.errorLog('Error serving page', err);
				res.status(500).json({error: 'Internal server error'});
			}
		});

		this.app.post('/api/admin/status', (req, res) => {
			if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
			if (!req.session.user.admin) return res.status(403).json({error: 'Not authorized'});
			const status = req.body;
			if (!this.webSocketServer.socket) return res.status(500).json({error: 'WebSocket server not connected'});
			try {
				this.webSocketServer.socket?.send(JSON.stringify({
					type: 'status',
					data: status,
				}));
				return res.status(200).json({success: true});
			} catch (err) {
				this.errorLog('Error sending status', err);
				return res.status(500).json({error: 'Internal server error'});
			}
		});

		this.app.post('/api/quizzes/new', async (req, res) => {
			if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
			try {
				const quizCount = await this.database.getUserQuizCount(req.session.user.id);
				if (quizCount >= 10) return res.status(403).json({error: 'Too many quizzes'});
				if (!validateQuiz(req.body)) {
					return res.status(400).json({error: 'Invalid quiz'});
				}
				const id = await this.database.makeQuiz(req.body, req.session.user);
				res.status(200).json({
					id,
				});
			} catch (err) {
				this.errorLog(`Error making quiz`, err);
				res.status(500).json({error: 'Internal server error'});
			}
		});

		this.app.delete('/api/quizzes/delete', async (req, res) => {
			if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
			const id = String(req.query.quizId);
			if (!id) return res.status(400).json({error: 'No quizId'});
			try {
				await this.database.deleteQuiz(id);
				res.status(200).json({success: true});
			} catch (err) {
				this.errorLog(`Error deleting quiz`, err);
				res.status(500).json({error: 'Internal server error'});
			}
		});

		this.app.post('/api/quizzes/edit', async (req, res) => {
			if (!req.session.user) return res.status(401).json({error: 'Not logged in'});
			const id = String(req.body.quizId);
			if (!id) return res.status(400).json({error: 'No quizId'});
			if (!validateQuiz(req.body)) {
				return res.status(400).json({error: 'Invalid quiz'});
			}
			try {
				await this.database.editQuiz(id, req.body, req.session.user);
				res.status(200).json({success: true});
			} catch (err) {
				this.errorLog(`Error editing quiz`, err);
				res.status(500).json({error: 'Internal server error'});
			}
		});
	}

	get time() {
		const date = new Date();
		return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
	}

	private createServer(): http.Server | https.Server {
		if (process.env.WEBHOOK_SSL_CERT && process.env.WEBHOOK_SSL_KEY) {
			this.debugLog('Starting webhook server with SSL');
			const cert = fs.readFileSync(`${__dirname}/../${process.env.WEBHOOK_SSL_CERT}`);
			const key = fs.readFileSync(`${__dirname}/../${process.env.WEBHOOK_SSL_KEY}`);
			return https.createServer({
				cert,
				key,
				ca: process.env.WEBHOOK_SSL_CA_CERT ? fs.readFileSync(`${__dirname}/../${process.env.WEBHOOK_SSL_CA_CERT}`) : undefined,
			}, this.app);
		} else {
			this.debugLog('Starting webhook server without SSL');
			return http.createServer(this.app);
		}
	}

	public startServer() {
		this.server.listen(process.env.PORT || 80, () => this.debugLog(`Opened on port ${process.env.PORT || 80}`));
	}

	public debugLog(message) {
		console.log(`[${chalk.blue('DEBUG')}][${chalk.yellow(this.time)}] ${message}`);
	}
	public warnLog(message) {
		console.log(`[${chalk.rgb(189, 183, 107)('WARN')}][${chalk.yellow(this.time)}] ${message}`);
	}
	public errorLog(message: string, error?: Error) {
		console.log(`[${chalk.red('ERROR')}][${chalk.yellow(this.time)}] ${message}${error ? `\n${error.stack}` : ''}`);
	}
}

function getServerSheildJson(label: string, value: string) {
	return {
		schemaVersion: 1,
		label,
		message: value,
		color: 'green',
	};
}
function getStatusSheildJson(value: boolean) {
	return {
		schemaVersion: 1,
		label: 'Status',
		message: value ? 'Online' : 'Offline',
		color: value ? 'green' : 'red',
	};
}
