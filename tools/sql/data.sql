INSERT INTO quizDifficulties (name, active) VALUES ('Easy', true);
INSERT INTO quizDifficulties (name, active) VALUES ('Medium', true);
INSERT INTO quizDifficulties (name, active) VALUES ('Hard', true);

INSERT INTO quizCategories (name, active) VALUES ('General Knowledge', true);
INSERT INTO quizCategories (name, active) VALUES ('Entertainment', true);
INSERT INTO quizCategories (name, active) VALUES ('Sports', true);
INSERT INTO quizCategories (name, active) VALUES ('Geography', true);
INSERT INTO quizCategories (name, active) VALUES ('History', true);
INSERT INTO quizCategories (name, active) VALUES ('Science', true);
INSERT INTO quizCategories (name, active) VALUES ('Art', true);
INSERT INTO quizCategories (name, active) VALUES ('Mythology', true);
INSERT INTO quizCategories (name, active) VALUES ('Politics', true);
INSERT INTO quizCategories (name, active) VALUES ('Animals', true);
INSERT INTO quizCategories (name, active) VALUES ('Vehicles', true);
INSERT INTO quizCategories (name, active) VALUES ('Computers', true);
INSERT INTO quizCategories (name, active) VALUES ('Celebrities', true);
INSERT INTO quizCategories (name, active) VALUES ('Film & Television', true);
INSERT INTO quizCategories (name, active) VALUES ('Video Games', true);
