cd "$( dirname "${BASH_SOURCE[0]}" )"

set -o allexport
source ../.env
set +o allexport

COMMAND=$(cat << EOF
DROP DATABASE $DB_DATABASE;
CREATE DATABASE IF NOT EXISTS $DB_DATABASE;
EOF
)

mysqlsh --user=$DB_USER --password=$DB_PASSWORD --host $DB_SERVER --sql -i <<< "$COMMAND"
