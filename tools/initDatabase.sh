cd "$( dirname "${BASH_SOURCE[0]}" )"

set -o allexport
source ../.env
set +o allexport

SQL_FILE=`cat ./sql/init.sql`

COMMAND=$(cat << EOF
CREATE DATABASE IF NOT EXISTS $DB_DATABASE;
USE $DB_DATABASE;
$SQL_FILE
EOF
)

mysqlsh --user=$DB_USER --password=$DB_PASSWORD --host $DB_SERVER --sql -i <<< "$COMMAND"
