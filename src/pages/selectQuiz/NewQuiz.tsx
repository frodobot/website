import Quiz from './Quiz';

export default function NewQuiz() {
	return (
		<Quiz newQuiz={true}/>
	);
}
