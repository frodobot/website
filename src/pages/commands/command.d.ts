import {React} from 'react';

export interface Command {
	name: string;
	actualCommandName?: string;
	description: string | React.Component;
	syntaxDescription: string;
	fields?: CommandField[];
	arguments?: Argument[];
	image: string;
}
interface subField {
	title: string | React.Component;
	value: string | React.Component;
}
interface Argument {
	name: string;
	description: string | React.Component;
}
