export interface SectionProps {
	children: React.ReactNode[];
	className?: string;
	style?: React.CSSProperties;
	nonResponsive?: boolean;
}
