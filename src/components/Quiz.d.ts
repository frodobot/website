export interface Quiz {
	quizId: string;
	displayId: string;
	name: string;
	image?: string;
	description: string;
	categoryId: string;
	difficultyId: string;
	lastChanged: string;
}

export interface QuizData {
	quizId: string;
	name: string;
	image?: string;
	description: string;
	questions: Question[];
	categoryId: string;
	difficultyId: string;
	lastChanged: string;
}

interface Question {
	questionId: string;
	question: string;
	incorrectAnswers: string[];
	correctAnswers: string[];
	lastChanged: string;
}
