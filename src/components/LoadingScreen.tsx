export default function LoadingScreen() {
	return (
		<section style={{height: 'unset', minHeight: '100vh'}}>
			<div className="center">
				<img src="/static/img/loading.svg" alt="Loading"/>
			</div>
		</section>
	);
}
