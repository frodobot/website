export function login() {
	window.location.href = `/api/discord/auth?redirect=${window.location.pathname}`;
}

export function logout() {
	window.location.href = `/api/discord/logout?redirect=${window.location.pathname}`;
}
